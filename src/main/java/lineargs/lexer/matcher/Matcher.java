package lineargs.lexer.matcher;

import java.util.List;

import javafx.util.Pair;

import lineargs.Option;

/**
 * It is a Lexer component in charge of matching some user defined options.
 */
public interface Matcher {
  /**
   * Tries to get a match, if nothing match pass the request to the next matcher.
   * @param partial the remaining text
   * @param ops the list of user defined Option
   * @return The matched Option and a list of its any values,
   paired with the length of the string matched
   */
  public Pair<Pair<Option, List<Object>>,Integer> verify(String partial, List<Option> ops);
}
