package lineargs;

import static lineargs.Types.NONE;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javafx.util.Pair;
import lineargs.lexer.Lexer;
import lineargs.printer.DefaultPrinter;
import lineargs.printer.Printer;

/**
 * An Object that represent the program in execution.
 */
public abstract class Program {
  String name;
  String description;
  HashMap<String,List<Object>> params;
  HashMap<Types,List<Object>> defaultParams;
  Lexer lex;
  Printer printer;

  /**
   * Creates an object that represent the program in execution.
   * @param name the name associated to the program in execution
   * @param description a description of the program in execution
   * @param args the list of command line arguments in input
   * @throws IllegalArgumentException if args contain some undefined options or not valid arguments
   */
  public Program(String name, String description, String[] args) {
    this.name = name;
    this.description = description;
    this.lex = new Lexer(getAllOptionWithHelp(),concat(args));
    params = new HashMap<>();
    defaultParams = new HashMap<>();
    Pair<Option, List<Object>> p;

    while ((p = lex.next()) != null) {
      if (p.getKey() instanceof DefaultOption) {
        List<Object> values = defaultParams.getOrDefault(p.getKey().getValue(),new ArrayList<>());
        values.addAll(p.getValue());
        defaultParams.put(p.getKey().getValue(),values);
      } else {
        params.put(p.getKey().getAlias(), p.getValue());
        params.put(p.getKey().getName(), p.getValue());
      }
    }
    this.printer = new DefaultPrinter(name, description, getAllOptionWithHelp());
  }

  /**
   * Sets a different Printer for the help message.
   * @param printer a different printer
   */
  public void setPrinter(Printer printer) {
    this.printer = printer;
  }

  /**
   * Return the list of all defined Options sorted by alias.
   * @return the list of all defined Options sorted by alias
   */
  public List<Option> getAllOptionWithHelp() {
    List<Option> res = getAllOptions();
    res.add(new Option("help","h",NONE,false, "Display this usage guide."));
    Collections.sort(res);
    return res;
  }

  /**
   * Return the list of user defined Options.
   * @return the list of user defined Options
   */
  public abstract List<Option> getAllOptions();

  /**
   * Checks if a parameter is set in program input arguments.
   * @param name the name or alias of a defined Option
   * @return true if an Option is set in program input
   */
  public boolean isParameterSet(String name) {
    return params.containsKey(name);
  }

  /**
   * Catches the values associated to a parameter.
   * @param name the name or alias of a defined Option
   * @return the values associated the the specified Option casted to the correct type
   */
  public List<Object> getParameterValues(String name) {
    return params.get(name);
  }

  /**
   * Checks if an argument is set in program input arguments.
   * @param value the type of the argument to be checked
   * @return true if a DefalutOption is set in program input.
   */
  public boolean isDefaultValueOfTypeSet(Types value) {
    return defaultParams.get(value) != null && defaultParams.get(value).size() > 0;
  }

  /**
   * Catches the values associated to a argument.
   * @param value the type of the input argument
   * @return the values associated to the arguments of the specified type
   */
  public List<Object> getDefaultValuesOfType(Types value) {
    return defaultParams.get(value);
  }

  /**
   * Build the program help message using the Printer specified.
   * @return a String that contains the help message associated to the executed program
   * @see lineargs.printer.Printer
   * @see lineargs.printer.DefaultPrinter
   */
  public String getHelpMessage() {
    return printer.prettyPrint();
  }

  protected static String concat(String[] args) {
    StringBuilder res = new StringBuilder();
    for (String a: args) {
      res.append(" " + (isOption(a) ? a : "\"" + a + "\""));
    }
    if (res.length() == 0) {
      return "";
    }
    return res.substring(1);
  }

  protected static boolean isOption(String in) {
    return in.matches("^-{1,2}[^\\s]+");
  }

}
