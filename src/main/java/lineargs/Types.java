package lineargs;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * A structure the represent the possible types of input arguments.
 */
public enum Types {
  STRING, INTEGER, DOUBLE, BOOLEAN, DATE, DATETIME, FILE, NONE;

  /**
   * Generates the regular expression associated to the specified Type.
   * It is used by the Lexer to identified all the options, arguments and respective values.
   * @param type the type of which you want to  get the regular expression
   * @return a regex expression for the specified type
   */
  public static String regexOfType(Types type) {
    switch (type) {
      case INTEGER:
        return "(\"?)[0-9]+(\")?";
      case DOUBLE:
        return "((\")?[0-9]+.[0-9]+(\")?|(\")?[0-9]+)(\")?";
      case BOOLEAN:
        return "((\")?[Tt][Rr][Uu][Ee](\")?|(\")?[Ff][Aa][Ll][Ss][Ee])(\")?";
      case DATE:
        return "(\")?[0-9]{4}-[0-9]{2}-[0-9]{2}(\")?";
      case DATETIME:
        return "(\")?[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}(\")?";
      default:
        return value();
    }
  }

  public static String value() {
    return "((\"[^\"]+\")|([^-][^\\s]*))";
  }

  /**
   * Converts the list of recognized String values to the specified types.
   * @param type the type to be associated to all the values identified by the Lexer
   * @param ls the list of values identified by the Lexer
   * @return a list of converted values to the specified type
   */
  public static List<Object> convert(Types type, List<String> ls) {
    switch (type) {
      case INTEGER:
        return Arrays.asList(ls.stream().map(Integer::parseInt).toArray());
      case DOUBLE:
        return Arrays.asList(ls.stream().map(Double::parseDouble).toArray());
      case BOOLEAN:
        return Arrays.asList(ls.stream().map(Boolean::parseBoolean).toArray());
      case DATE:
        return Arrays.asList(ls.stream().map(x -> dateOfString("yyyy-MM-dd",x))
            .toArray());
      case DATETIME:
        return Arrays.asList(ls.stream().map(x -> dateOfString("yyyy-MM-dd HH:mm:ss", x))
            .toArray());
      case FILE:
        return Arrays.asList(ls.stream().map(File::new).toArray());
      default:
        return Arrays.asList(ls.stream().toArray());
    }
  }

  protected static Date dateOfString(String pattern, String s) {
    SimpleDateFormat df = new SimpleDateFormat(pattern);
    try {
      return df.parse(s);
    } catch (ParseException e) {
      System.err.println(e.getMessage());
      return null;
    }
  }
}
