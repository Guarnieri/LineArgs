package lineargs.lexer.matcher;

import java.util.List;

import javafx.util.Pair;

import lineargs.Option;

/**
 * It is a Lexer component in charge of returning a null Option
 when the Lexer finished to analyze the input string.
 */
public class EndMatcher implements Matcher {

  /**
   * Return a null Option after the lexer finished to analyzed the input string.
   * @param partial the remaining text
   * @param ops the list of user defined Option
   * @return a null Option
   */
  @Override
  public Pair<Pair<Option, List<Object>>,Integer> verify(String partial, List<Option> ops) {
    return new Pair(null,0);
  }

}
