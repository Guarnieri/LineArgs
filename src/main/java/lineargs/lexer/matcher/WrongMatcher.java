package lineargs.lexer.matcher;

import java.util.List;

import javafx.util.Pair;

import lineargs.Option;

/**
 * It is a Lexer component in charge of throws an exception
 when nothing match the remaining text.
 */
public class WrongMatcher implements Matcher {
  private final Matcher next;

  /**
   * Create a new instance of the Matcher.
   * @param next the next Matcher to invoke if no match found
   */
  public WrongMatcher(Matcher next) {
    this.next = next;
  }

  /**
   * Check if the input string contains a sequence of character not recognized by the lexer.
   * @param partial the remaining text
   * @param ops the list of user defined Option
   * @return if the remaining text is empty returns next Matcher output
   * @throws IllegalArgumentException if nothing match with the remained text
   */
  @Override
  public Pair<Pair<Option, List<Object>>,Integer> verify(String partial, List<Option> ops) {
    if (!partial.equals("")) {
      throw new IllegalArgumentException("\"" + partial + "\" not a valid parameter");
    }
    return next.verify(partial, ops);
  }
}
