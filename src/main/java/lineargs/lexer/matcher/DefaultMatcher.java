package lineargs.lexer.matcher;

import static lineargs.Types.convert;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javafx.util.Pair;

import lineargs.DefaultOption;
import lineargs.Option;

/**
 * It is a Lexer component in charge of matching one of the user defined arguments
 with the remained text.
 */
public class DefaultMatcher implements Matcher {
  Matcher next;

  /**
   * Create a new instance of the Matcher.
   * @param next the next Matcher to invoke if no match found
   */
  public DefaultMatcher(Matcher next) {
    this.next = next;
  }

  /**
   * Tries to match an user define DefaultOption with the remained text,
   if nothing match it passes the request to another Matcher.
   * @param partial the remaining text
   * @param ops the list of user defined Option
   * @return the DefaultOption matched, his value and the length of the matched string
   * @see lineargs.DefaultOption
   */
  @Override
  public Pair<Pair<Option, List<Object>>, Integer> verify(String partial, List<Option> ops) {
    for (Option o: ops) {
      if (o instanceof DefaultOption) {
        Pattern pattern = Pattern.compile(o.getAliasRegex());
        java.util.regex.Matcher matcher = pattern.matcher(partial);
        if (matcher.find()) {
          String res = matcher.group(0);
          List<String> lo = new ArrayList<>();
          lo.add(cleanParameter(res));
          return new Pair<>(new Pair<>(o,convert(o.getValue(),lo)),res.length());
        }
      }
    }
    return next.verify(partial,ops);
  }

  protected static String cleanParameter(String value) {
    return value.trim().replaceFirst("^\"","").replaceFirst("\"$","");
  }
}
