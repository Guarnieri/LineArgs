package lineargs.lexer.matcher;

import static lineargs.Types.convert;
import static lineargs.Types.value;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javafx.util.Pair;

import lineargs.DefaultOption;
import lineargs.Option;

/**
 * It is a Lexer component in charge of matching one of the user defined option
 with the remained text.
 */
public class RegularMatcher implements Matcher {
  private final Matcher next;

  /**
   * Create a new instance of the Matcher.
   * @param next the next Matcher to invoke if no match found
   */
  public RegularMatcher(Matcher next) {
    this.next = next;
  }

  /**
   * Tries to match an user define Option with the remained text,
   if nothing match it passes the request to another Matcher.
   * @param partial the remaining text
   * @param ops the list of user defined Option
   * @return The matched Option and a list of its any values,
   paired with the length of the string matched
   * @see lineargs.Option
   */
  @Override
  public Pair<Pair<Option, List<Object>>,Integer> verify(String partial, List<Option> ops) {
    int max = 0;
    String res = null;
    Option option = null;
    for (int i = 0; i < ops.size(); i++) {
      String cur = matchString(partial,ops.get(i));
      if (cur.length() > max) {
        max = cur.length();
        res = cur;
        option = ops.get(i);
      }
    }
    if (res != null) {
      return new Pair(
          new Pair(option, convert(option.getValue(), getParameters(res))),
          res.length()
      );
    }
    return next.verify(partial, ops);
  }

  private static String matchString(String in, Option option) {
    if (option instanceof DefaultOption) {
      return "";
    }
    Pattern pattern = Pattern.compile(option.getAliasRegex());
    java.util.regex.Matcher matcher = pattern.matcher(in);
    if (matcher.find()) {
      return matcher.group(0);
    }
    pattern = Pattern.compile(option.getNameRegex());
    matcher = pattern.matcher(in);
    if (matcher.find()) {
      return matcher.group(0);
    }
    return "";
  }

  protected static String cleanParameter(String value) {
    return value.trim().replaceFirst("^\"","").replaceFirst("\"$","");
  }

  protected static List<String> getParameters(String in) {
    ArrayList<String> ls = new ArrayList<>();
    Pattern pattern = Pattern.compile(" " + value());
    java.util.regex.Matcher matcher = pattern.matcher(in);
    while (matcher.find()) {
      ls.add(cleanParameter(matcher.group(0)));
    }
    return ls;
  }

}
