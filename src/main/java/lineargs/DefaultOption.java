package lineargs;

import static lineargs.Types.NONE;
import static lineargs.Types.regexOfType;

/**
 * An object that represent an input arguments.
 */
public class DefaultOption extends Option {

  public DefaultOption(Types value, String description) {
    super("", "", value, false, description);
    if (value == NONE) {
      throw new IllegalArgumentException("You cannot define a NONE type in a DefaultOption.");
    }
  }

  /**
   * Return a regular expression used by the Lexer to recognise the argument.
   * @return a regular expression that represent the argument
   * @see lineargs.lexer.Lexer
   */
  @Override
  public String getNameRegex() {
    return "^" + regexOfType(this.getValue()) + "($|\\s)";
  }

  /**
   * Return a regular expression used by the Lexer to recognise the argument.
   * @return a regular expression that represent the argument
   * @see lineargs.lexer.Lexer
   */
  @Override
  public String getAliasRegex() {
    return "^" + regexOfType(this.getValue()) + "($|\\s)";
  }

}
