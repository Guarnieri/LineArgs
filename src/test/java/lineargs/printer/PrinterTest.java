package lineargs.printer;

import static lineargs.Types.DATETIME;
import static lineargs.Types.INTEGER;
import static lineargs.Types.NONE;
import static lineargs.Types.STRING;
import static lineargs.printer.ConsoleColors.RED;
import static lineargs.printer.DefaultPrinter.format;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import lineargs.DefaultOption;
import lineargs.Option;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;

public class PrinterTest {
  @Test
  public void testFormat() {
    assertThat(format(RED,"some text")).isEqualTo("\033[0;31msome text\033[0m");
  }

  @Test
  public void testMaxAliasLength() {
    ArrayList<Option> ops = new ArrayList<>();
    ops.add(new Option("source","src",STRING,true,""));
    ops.add(new Option("verbose","v",NONE,false,""));
    ops.add(new Option("sum","s",INTEGER,true,""));
    SoftAssertions soft = new SoftAssertions();
    soft.assertThat(DefaultPrinter.maxStringLength(ops,Option::getAlias)).isEqualTo(3);
    soft.assertThat(DefaultPrinter.maxStringLength(ops,Option::getName)).isEqualTo(7);
    soft.assertThat(DefaultPrinter.maxStringLength(ops,x -> x.getValue().toString())).isEqualTo(7);
    soft.assertAll();
  }

  @Test
  public void testFillWithSpaces() {
    assertThat(DefaultPrinter.fillWithSpaces("source", 4)).isEqualTo("source    ");
  }

  @Test
  public void testFormatLine() {
    Option option = new Option("source","src",STRING,true,"option description");
    List<Option> ops = new ArrayList<>();
    ops.add(option);
    DefaultPrinter p = new DefaultPrinter("name","description",ops);
    String  line = p.formatLine(
        option.getName(),
        option.getAlias(),
        option.getValue().toString(),
        option.getDescription()
    );
    SoftAssertions soft = new SoftAssertions();
    soft.assertThat(line).contains("source");
    soft.assertThat(line).contains("src");
    soft.assertThat(line).contains("STRING");
    soft.assertThat(line).contains("option description");
    soft.assertAll();
  }

  @Test
  public void testNotNullPrettyPrint() {
    Option option = new Option("source","src",STRING,true,"option description");
    List<Option> ops = new ArrayList<>();
    ops.add(option);
    DefaultPrinter p = new DefaultPrinter("name","description",ops);
    assertThat(p.prettyPrint()).isNotNull();
  }

  @Test
  public void testFormatLineSize() {
    Option opt1 = new Option("source","src",STRING,true,"");
    Option opt2 = new Option("sum","s",INTEGER,true,"");
    ArrayList<Option> ops = new ArrayList<>();
    ops.add(opt1);
    ops.add(opt2);
    DefaultPrinter p = new DefaultPrinter("name","description",ops);
    SoftAssertions soft = new SoftAssertions();
    soft.assertThat(p.formatLine(
        opt1.getName(),
        opt1.getAlias(),
        opt1.getValue().toString(),
        opt1.getDescription()
    ).length()).isEqualTo(73);
    soft.assertThat(p.formatLine(
        opt2.getName(),
        opt2.getAlias(),
        opt2.getValue().toString(),
        opt2.getDescription()
    ).length()).isEqualTo(73);
    soft.assertAll();
  }

  @Test
  public void testPrettyPrint() {
    Option opt = new Option("source","src",NONE,true,"");
    ArrayList<Option> ops = new ArrayList<>();
    ops.add(opt);
    DefaultPrinter p = new DefaultPrinter("name","description",ops);
    String line = p.formatLine(opt.getName(), opt.getAlias(), "", opt.getDescription());
    assertThat(p.prettyPrint()).contains(line);
  }

  @Test
  public void testPrettyPrintDefaultOption() {
    Option opt = new DefaultOption(STRING,"description");
    ArrayList<Option> ops = new ArrayList<>();
    ops.add(opt);
    DefaultPrinter p = new DefaultPrinter("name","description",ops);
    String line = p.formatLine(
        opt.getName(),
        opt.getAlias(),
        opt.getValue().toString(),
        opt.getDescription()
    ).replaceAll("[-,]"," ");
    assertThat(p.prettyPrint()).contains(line);
  }

  @Test
  public void testPrettyPrintMultiValues() {
    Option opt = new Option("source","src",STRING,true,"");
    ArrayList<Option> ops = new ArrayList<>();
    ops.add(opt);
    DefaultPrinter p = new DefaultPrinter("name","description",ops);
    String line = p.formatLine(
        opt.getName(),
        opt.getAlias(),
        opt.getValue().toString() + DefaultPrinter.multiValueStr,
        opt.getDescription()
    );
    assertThat(p.prettyPrint()).contains(line);
  }

  @Test
  public void testPrettyPrintSingleValues() {
    Option opt = new Option("source","src",DATETIME,true,"");
    ArrayList<Option> ops = new ArrayList<>();
    ops.add(opt);
    DefaultPrinter p = new DefaultPrinter("name","description",ops);
    String line = p.formatLine(
        opt.getName(),
        opt.getAlias(),
        opt.getValue() + DefaultPrinter.multiValueStr,
        opt.getDescription()
    );
    assertThat(p.prettyPrint()).contains(line);
  }

}
