package lineargs.lexer.matcher;

import static lineargs.Types.NONE;

import java.util.List;
import java.util.regex.Pattern;

import javafx.util.Pair;

import lineargs.Option;

/**
 * It is a Lexer component in charge of matching multi alias parameters.
 */
public class MultiAliasMatcher implements Matcher {
  private final Matcher next;

  /**
   * Create a new instance of the Matcher.
   * @param next the next Matcher to invoke if no match found
   */
  public MultiAliasMatcher(Matcher next) {
    this.next = next;
  }


  /**
   * Tries to match a multi alias parameter,
   if nothing match it passes the request to another Matcher.
   * A multi alias parameter is a parameter formed by the union
   of single character alias from different NONE typed options.
   * @param partial the remaining text
   * @param ops the list of user defined Option
   * @return The matched Option paired with -1
   * @see lineargs.Option
   */
  @Override
  public Pair<Pair<Option, List<Object>>,Integer> verify(String partial, List<Option> ops) {
    if (!partial.equals("")) {
      Pattern pattern = Pattern.compile("^-[^-][^\\s]*");
      java.util.regex.Matcher matcher = pattern.matcher(partial);
      if (matcher.find()) {
        String matched = matcher.group(0).substring(1);
        boolean isMultiAlias = true;
        for (char c : matched.toCharArray()) {
          isMultiAlias &= (checkIsMultiAlias(c,ops) != null);
        }
        if (isMultiAlias) {
          return new Pair<>(new Pair<>(checkIsMultiAlias(matched.charAt(0),ops),null),-1);
        }

      }
    }
    return next.verify(partial, ops);
  }

  protected static Option checkIsMultiAlias(char singleAlias, List<Option> ls) {
    for (Option opt : ls) {
      if (("" + singleAlias).equals(opt.getAlias()) && opt.getValue() == NONE) {
        return opt;
      }
    }
    return null;
  }
}
