package lineargs.lexer;

import java.util.List;

import javafx.util.Pair;
import lineargs.Option;
import lineargs.lexer.matcher.DefaultMatcher;
import lineargs.lexer.matcher.EndMatcher;
import lineargs.lexer.matcher.Matcher;
import lineargs.lexer.matcher.MultiAliasMatcher;
import lineargs.lexer.matcher.RegularMatcher;
import lineargs.lexer.matcher.WrongMatcher;

/**
 * A class that implements a lexical analyzer as an Iterator.
 * Given a string that represent the list of arguments passed in input to the program,
 this returns the matched Option step by step.
 */
public class Lexer {
  List<Option> ops;
  String input;
  String partial;
  Matcher matcher;

  /**
   * Creates a new instance of the lexical analyzer.
   * @param ops the list of used defined Options
   * @param input a string that represents the list of arguments passed to the executed program
   */
  public Lexer(List<Option> ops, String input) {
    this.ops = ops;
    this.input = input;
    this.partial = input;
    this.matcher = new RegularMatcher(
        new MultiAliasMatcher(
            new DefaultMatcher(
                new WrongMatcher(
                    new EndMatcher()
                )
            )
        )
    );
  }

  /**
   * Tries to match an option with the remained input string.
   * @return null if it finished to analyze the input string,
   otherwise the matching option and his eventually values associated
   * @throws IllegalArgumentException if not found any matching
   */
  public Pair<Option,List<Object>> next() {
    Pair<Pair<Option, List<Object>>,Integer> res = matcher.verify(partial,ops);
    if (res.getValue() >= 0) {
      this.partial = partial.substring(res.getValue()).trim();
    } else {
      this.partial = Option.aliasPrefix + partial.substring(2);
    }
    return res.getKey();
  }
}
