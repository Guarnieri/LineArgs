package lineargs.printer;

import static lineargs.Types.NONE;
import static lineargs.printer.ConsoleColors.BLUE;
import static lineargs.printer.ConsoleColors.GREEN_BOLD;
import static lineargs.printer.ConsoleColors.RED_BOLD;
import static lineargs.printer.ConsoleColors.RED_UNDERLINED;
import static lineargs.printer.ConsoleColors.RESET;
import static lineargs.printer.ConsoleColors.YELLOW;

import java.util.List;
import java.util.function.Function;

import lineargs.DefaultOption;
import lineargs.Option;

/**
 * An object that formats and prints the help message associated to the executed program.
 */
public class DefaultPrinter implements Printer {
  String name;
  String description;
  List<Option> ls;
  int aliasLen;
  int nameLen;
  int valueLen;

  public static final String multiValueStr = "...";

  /**
   * Create a new instance of a Printer Object.
   * @param name the name associated to the executed program
   * @param description a description of the executed program
   * @param ls the list of Option defined for the executed program
   */
  public DefaultPrinter(String name, String description, List<Option> ls) {
    this.name = name;
    this.description = description;
    this.ls = ls;
    this.aliasLen = maxStringLength(ls, Option::getAlias);
    this.nameLen = maxStringLength(ls, Option::getName);
    this.valueLen = maxStringLength(ls,
        x -> x.getValue().toString() + (x.isMultiple() ? multiValueStr : "")
    );
  }

  /**
   * Produces a formatted help message.
   * @return a formatted string that represent the help message of the executed program
   */
  @Override
  public String prettyPrint() {

    String text = String.format("%s\n\n  %s\n\n%s\n\n",
        format(RED_UNDERLINED,name),
        format(BLUE,description),
        format(RED_UNDERLINED,"Arguments:")
    );
    for (Option o : ls) {
      if (o instanceof DefaultOption) {
        text +=
            formatLine(
                o.getName(),
                o.getAlias(),
                (o.getValue().toString()),
                o.getDescription()
            ).replaceAll("[-,]"," ") + "\n";
      }
    }
    text += format(RED_UNDERLINED,"Options:\n\n");
    for (Option o : ls) {
      if (!(o instanceof DefaultOption)) {
        text +=
            formatLine(
                o.getName(),
                o.getAlias(),
                (o.getValue() != NONE ? o.getValue() + (o.isMultiple() ? multiValueStr : "") : ""),
                o.getDescription()
            ) + "\n";
      }
    }
    return text;
  }

  protected String formatLine(String name, String alias, String type, String description) {
    return String.format("  %s%s%s%s\n",
        fillWithSpaces(format(RED_BOLD,Option.aliasPrefix + alias) + ", ",
            aliasLen - alias.length()),
        fillWithSpaces(format(GREEN_BOLD, Option.namePrefix + name) + " ",nameLen - name.length()),
        fillWithSpaces(format(YELLOW,type) + " ",valueLen - type.length()),
        format(BLUE,description));

  }

  protected static String format(String color, String s) {
    return color + s + RESET;
  }

  protected static String fillWithSpaces(String str, int diff) {
    return str + new String(new char[diff]).replace("\0", " ");
  }

  protected static int maxStringLength(List<Option> ls, Function<Option,String> f) {
    return ls.stream().map(x -> f.apply(x).length()).reduce(Math::max).get();
  }
}
