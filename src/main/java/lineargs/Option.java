package lineargs;

import static lineargs.Types.NONE;
import static lineargs.Types.regexOfType;

/**
 * An object the represent an input parameter.
 */
public class Option implements Comparable<Option> {

  private final String name;
  private final String alias;
  private final Types value;
  private final boolean multiple;
  private final String description;

  public static final String namePrefix = "--";
  public static final String aliasPrefix = "-";

  /**
   * Creates a new input parameter.
   * @param name the name associated to the parameter
   * @param alias the alias associated to the parameter
   * @param value the type associated to the parameter values (NONE if not accept parameters)
   * @param multiple true if the parameter accepts multiple values
   * @param description a description of the parameter
   */
  public Option(String name, String alias, Types value, boolean multiple, String description) {
    this.name = name;
    this.alias = alias;
    this.value = value;
    this.multiple = multiple;
    this.description = description;
  }

  /**
   * Return a regular expression used by the Lexer to recognise the parameter by name.
   * @return a regular expression that represent the parameter
   * @see lineargs.lexer.Lexer
   */
  public String getNameRegex() {
    return String.format("^%s%s%s",
        namePrefix,
        this.name,
        this.getValue() == NONE ? "($|\\s)" :
            (this.isMultiple() ? multiple(regexOfType(this.getValue())) :
                regexOfType(this.getValue())));
  }

  /**
   * Return a regular expression used by the Lexer to recognise the parameter by alias.
   * @return a regular expression that represent the parameter
   * @see lineargs.lexer.Lexer
   */
  public String getAliasRegex() {
    return String.format("^%s%s%s",
        aliasPrefix,
        this.alias,
        this.getValue() == NONE ? "($|\\s)" :
            (this.isMultiple() ? multiple(regexOfType(this.getValue())) :
                regexOfType(this.getValue())));
  }

  protected static String multiple(String expr) {
    return "(\\s" + expr + ")+";
  }

  /**
   * Compares the alias of the current Option with the alias of another Option.
   * @param o an other Option to compares with
   * @return 0 if the alias are equals, a positive integer if the
   current alias is greater then the other, a negative integer otherwise
   */
  @Override
  public int compareTo(Option o) {
    return alias.compareTo(o.alias);
  }

  /**
   * Return the name associated to the parameter.
   * @return the name associated to the parameter
   */
  public String getName() {
    return this.name;
  }

  /**
   * Retun the alias associated to the parameter.
   * @return the alias associated to the parameter
   */
  public String getAlias() {
    return alias;
  }

  /**
   * Return the description associated to the parameter.
   * @return the description associated to the parameter
   */
  public String getDescription() {
    return description;
  }

  /**
   * Return the type associated to the parameter.
   * @return the type associated to the parameter
   */
  public Types getValue() {
    return value;
  }

  /**
   * Return true if the parameter accepts multiple values.
   * @return true if the parameter accepts multiple values
   */
  public boolean isMultiple() {
    return multiple;
  }
}
