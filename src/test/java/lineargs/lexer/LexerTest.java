package lineargs.lexer;

import static lineargs.Types.BOOLEAN;
import static lineargs.Types.DATE;
import static lineargs.Types.DATETIME;
import static lineargs.Types.DOUBLE;
import static lineargs.Types.FILE;
import static lineargs.Types.NONE;
import static lineargs.Types.STRING;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javafx.util.Pair;

import lineargs.DefaultOption;
import lineargs.Option;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;

public class LexerTest {

  @Test
  public void testLexer() {
    ArrayList<Option> lo = new ArrayList<Option>();
    lo.add(new Option("source","src",STRING,true,""));
    lo.add(new Option("verbose","v",NONE,false,""));
    Lexer l = new Lexer(lo,"--source dir1/file1.txt \"dir2/file2.txt\" -v");
    SoftAssertions soft = new SoftAssertions();
    Pair<Option,List<Object>> parz = l.next();
    soft.assertThat(parz.getKey().getName()).isEqualTo("source");
    soft.assertThat(parz.getValue().get(0)).isEqualTo("dir1/file1.txt");
    soft.assertThat(parz.getValue().get(1)).isEqualTo("dir2/file2.txt");
    parz = l.next();
    soft.assertThat(parz.getKey().getAlias()).isEqualTo("v");
    soft.assertThat(parz.getValue()).isEmpty();
    soft.assertAll();
  }

  @Test
  public void testWrongArgumentLexer() {
    ArrayList<Option> lo = new ArrayList<Option>();
    lo.add(new Option("source","src",STRING,true,""));
    lo.add(new Option("verbose","v",NONE,false,""));
    Lexer l = new Lexer(lo,"--file");
    assertThatThrownBy(() -> l.next()).isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void testLexerMultiMatch() {
    ArrayList<Option> lo = new ArrayList<Option>();
    lo.add(new Option("source","src",STRING,true,""));
    lo.add(new Option("source","src",NONE,false,""));
    Lexer l = new Lexer(lo,"--source dir1/file1.txt \"dir2/file2.txt\" -src -src dir3/file f3");
    SoftAssertions soft = new SoftAssertions();
    Pair<Option,List<Object>> parz = l.next();
    soft.assertThat(parz.getKey().getName()).isEqualTo("source");
    soft.assertThat(parz.getValue().get(1)).isEqualTo("dir2/file2.txt");
    parz = l.next();
    soft.assertThat(parz.getKey().getAlias()).isEqualTo("src");
    soft.assertThat(parz.getValue()).isEmpty();
    parz = l.next();
    soft.assertThat(parz.getKey().getAlias()).isEqualTo("src");
    soft.assertThat(parz.getValue().get(0)).isEqualTo("dir3/file");
    soft.assertThat(l.next()).isNull();
    soft.assertAll();
  }

  @Test
  public void testLexerDoubleParameter() {
    ArrayList<Option> lo = new ArrayList<Option>();
    lo.add(new Option("values","val",DOUBLE,true,""));
    Lexer l = new Lexer(lo,"--values 2.3 4.5 \"10.2\" 3");
    SoftAssertions soft = new SoftAssertions();
    Pair<Option,List<Object>> parz = l.next();
    soft.assertThat(parz.getKey().getName()).isEqualTo("values");
    soft.assertThat((double)parz.getValue().get(0) + (double)parz.getValue().get(2))
        .isEqualTo(12.5);
    soft.assertThat(l.next()).isNull();
    soft.assertAll();
  }

  @Test
  public void testLexerBooleanParameter() {
    ArrayList<Option> lo = new ArrayList<Option>();
    lo.add(new Option("values","val",BOOLEAN,true,""));
    Lexer l = new Lexer(lo,"--values true \"false\"");
    SoftAssertions soft = new SoftAssertions();
    Pair<Option,List<Object>> parz = l.next();
    soft.assertThat(parz.getKey().getName()).isEqualTo("values");
    soft.assertThat((boolean) parz.getValue().get(0)).isTrue();
    soft.assertThat((boolean) parz.getValue().get(1)).isFalse();
    soft.assertThat(l.next()).isNull();
    soft.assertAll();
  }

  @Test
  public void testLexerDateParameter() {
    ArrayList<Option> lo = new ArrayList<Option>();
    lo.add(new Option("values","val",DATE,true,""));
    Lexer l = new Lexer(lo,"--values 2018-06-15 \"2018-06-16\"");
    SoftAssertions soft = new SoftAssertions();
    Pair<Option,List<Object>> parz = l.next();
    soft.assertThat(parz.getKey().getName()).isEqualTo("values");
    soft.assertThat(((Date)parz.getValue().get(0))).hasDayOfMonth(15);
    soft.assertThat(l.next()).isNull();
    soft.assertAll();
  }

  @Test
  public void testLexerWrongDateParameter() {
    ArrayList<Option> lo = new ArrayList<Option>();
    lo.add(new Option("values","val",DATE,true,""));
    Lexer l = new Lexer(lo,"--values \"2018 06 15\"");
    assertThatThrownBy(() -> l.next()).isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void testLexerDateTimeParameter() {
    ArrayList<Option> lo = new ArrayList<Option>();
    lo.add(new Option("values","val",DATETIME,true,""));
    Lexer l = new Lexer(lo,"--values \"2018-06-15 14:20:56\"");
    SoftAssertions soft = new SoftAssertions();
    Pair<Option,List<Object>> parz = l.next();
    soft.assertThat(parz.getKey().getName()).isEqualTo("values");
    soft.assertThat(((Date)parz.getValue().get(0))).hasDayOfMonth(15);
    soft.assertThat(l.next()).isNull();
    soft.assertAll();
  }

  @Test
  public void testLexerFileParameter() {
    ArrayList<Option> lo = new ArrayList<Option>();
    lo.add(new Option("values","val",FILE,true,""));
    Lexer l = new Lexer(lo,"--values \"dir/file1.txt\" dir/file2.txt");
    SoftAssertions soft = new SoftAssertions();
    Pair<Option,List<Object>> parz = l.next();
    soft.assertThat(parz.getKey().getName()).isEqualTo("values");
    soft.assertThat(((File) parz.getValue().get(0)).getPath()).isEqualTo("dir/file1.txt");
    soft.assertThat(((File) parz.getValue().get(1)).getPath()).isEqualTo("dir/file2.txt");
    soft.assertThat(l.next()).isNull();
    soft.assertAll();
  }

  @Test
  public void testLexerMultiTypeParameter() {
    ArrayList<Option> lo = new ArrayList<Option>();
    lo.add(new Option("values","val",DATE,true,""));
    lo.add(new Option("values","val",DATETIME,true,""));
    Lexer l = new Lexer(lo,"--values \"2018-06-16\" 2018-06-15");
    Lexer l1 = new Lexer(lo, "-val \"2018-06-16 13:34:51\"");
    SoftAssertions soft = new SoftAssertions();
    Pair<Option,List<Object>> parz = l.next();
    Pair<Option,List<Object>> parz1 = l1.next();
    soft.assertThat(parz.getKey().getName()).isEqualTo("values");
    soft.assertThat(((Date)parz.getValue().get(0))).hasDayOfMonth(16);
    soft.assertThat(((Date)parz1.getValue().get(0))).hasDayOfMonth(16);
    soft.assertThat(l.next()).isNull();
    soft.assertAll();
  }

  @Test
  public void testMultiAliasParameter() {
    ArrayList<Option> lo = new ArrayList<Option>();
    lo.add(new Option("verbose","v",NONE,false,""));
    lo.add(new Option("recursive","r",NONE,false,""));
    lo.add(new Option("values","val",FILE,true,""));
    Lexer l = new Lexer(lo,"-v -r -vr");
    SoftAssertions soft = new SoftAssertions();
    soft.assertThat(l.next().getKey().getName()).isEqualTo("verbose");
    soft.assertThat(l.next().getKey().getName()).isEqualTo("recursive");
    soft.assertThat(l.next().getKey().getName()).isEqualTo("verbose");
    soft.assertThat(l.next().getKey().getName()).isEqualTo("recursive");
    soft.assertAll();
  }

  @Test
  public void testWrongMultiAliasParameter() {
    ArrayList<Option> lo = new ArrayList<Option>();
    lo.add(new Option("verbose","v",NONE,false,""));
    lo.add(new Option("date","d",DATE,false,""));
    lo.add(new Option("recursive","r",NONE,false,""));
    lo.add(new Option("values","val",FILE,true,""));
    Lexer l = new Lexer(lo,"-v -r -vrd");
    SoftAssertions soft = new SoftAssertions();
    soft.assertThat(l.next().getKey().getName()).isEqualTo("verbose");
    soft.assertThat(l.next().getKey().getName()).isEqualTo("recursive");
    soft.assertThatThrownBy(() -> l.next()).isInstanceOf(IllegalArgumentException.class);
    soft.assertAll();
  }

  @Test
  public void testNotMultiAliasParameter() {
    ArrayList<Option> lo = new ArrayList<Option>();
    lo.add(new Option("verbose","v",NONE,false,""));
    lo.add(new Option("recursive","r",NONE,false,""));
    lo.add(new Option("values","val",FILE,true,""));
    Lexer l = new Lexer(lo,"-v -r vr");
    SoftAssertions soft = new SoftAssertions();
    soft.assertThat(l.next().getKey().getName()).isEqualTo("verbose");
    soft.assertThat(l.next().getKey().getName()).isEqualTo("recursive");
    soft.assertThatThrownBy(() -> l.next()).isInstanceOf(IllegalArgumentException.class);
    soft.assertAll();
  }

  @Test
  public void testDefaultOption() {
    ArrayList<Option> lo = new ArrayList<Option>();
    lo.add(new Option("verbose","v",NONE,false,""));
    lo.add(new Option("recursive","r",STRING,true,""));
    lo.add(new DefaultOption(DOUBLE,""));
    lo.add(new DefaultOption(DATE,""));
    Lexer l = new Lexer(lo,"-r \"value of r\" -v 3.14 \"2018-06-17\" \"3.14\"");
    SoftAssertions soft = new SoftAssertions();
    soft.assertThat(l.next().getKey().getName()).isEqualTo("recursive");
    soft.assertThat(l.next().getKey().getName()).isEqualTo("verbose");
    Pair<Option, List<Object>> parz = l.next();
    soft.assertThat(parz.getKey()).isInstanceOf(DefaultOption.class);
    soft.assertThat(parz.getValue().get(0)).isEqualTo(3.14);
    parz = l.next();
    soft.assertThat(parz.getKey()).isInstanceOf(DefaultOption.class);
    soft.assertThat(parz.getKey().getValue()).isEqualTo(DATE);
    soft.assertThat((Date) parz.getValue().get(0)).hasDayOfMonth(17);
    soft.assertThat(l.next().getValue().get(0)).isEqualTo(3.14);
    soft.assertAll();
  }



}

