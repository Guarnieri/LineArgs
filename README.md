# LineArgs
[![pipeline status](https://gitlab.com/Guarnieri/LineArgs/badges/master/pipeline.svg)](https://gitlab.com/Guarnieri/LineArgs/commits/master) [![coverage report](https://gitlab.com/Guarnieri/LineArgs/badges/master/coverage.svg)](https://gitlab.com/Guarnieri/LineArgs/commits/master)

LineArgs is a simple Java command line interface.
It is possible specifying many arguments or parameters by using the classes DefaultOption and Option.

Each arguments and parameter can have one of the following types:
* **STRING**: catch any string, return a String object
* **INTEGER**: catch any integer number, return an integer value
* **DOUBLE**: catch any integer or decimal number separated by ".", return a double value
* **BOOLEAN**: catch true or false values, return a boolean value
* **DATE**: catch any date specified by yyyy-MM-dd notation, return a Date object
* **DATETIME**: catch any date specified by yyyy-MM-dd HH:mm:ss notation, return a Date object
* **FILE**: catch any string, return a File object
* **NONE**

It is possible to associate multiple values to a single parameter by setting the attribute ***multi*** to true.

At the same time LineArgs adds the help command and produce the help message to be printed.
It is also possible redefine this message creating a new class that implements the Printer interface.

# Code Example
Next code shows how to configure LineArgs to catch program parameters and arguments:
```java
public static void main(String[] args) {
	Program p = new Program("gcc","It Normally does preprocessing, compilation, assembly and linking.", args) {
      @Override
      public List<Option> getAllOptions() {
        List<Option> ls = new ArrayList<>();
		ls.add(new DefaultOption(FILE,"input file"));
        ls.add(new Option("output", "o", FILE, false, "Place output in file."));
        ls.add(new Option("verbose", "v", NONE,false, "Print the commands executed."));
        return ls;
      }
    }
}
```

with the previous code you can call your program for instance in the following ways:
```console
gcc inputfile
gcc inputfile -o outputfile
gcc inputfile --output outputfile
gcc input -o outputfile --verbose
...
and so on
```

It is easily possible verifying if a parameter is set using:
```java
//search by name
boolean set0 = program.isParameterSet("verbose");

//search by alias
boolean set1 = program.isParameterSet("v");
```

It is also easily possible to get the values associated to any parameter:
```java
//get by name
List<Object> out0 = program.getParameterValues("output");

//get by alias
List<Object> out1 = program.getParameterValues("o");
```
If you want instead the value associated to an argument you need to execute the following code:
```java
List<Object> nums = program.getDefaultValuesOfType(FILE);
```

The following instruction produce the help message associated to your program:
```java
String msg = program.getHelpMessage();
```

# Installation
Clone the repository using
```console
git@gitlab.com:Guarnieri/LineArgs.git
```
or
```console
https://gitlab.com/Guarnieri/LineArgs.git
```
if you don't have any ssh key.

To compile the library type the follow commands

```console
cd LineArgs
./gradlew jar
```
You can find the jar file in build/libs.

# Documentation

You can get all the documentation typing the follow command
```console
./gradlew javaDoc
```
You can find the JavaDocs in build/docs/javadoc.
