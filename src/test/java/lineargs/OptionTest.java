package lineargs;

import static lineargs.Types.NONE;
import static lineargs.Types.STRING;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;

public class OptionTest {

  @Test
  public void testOption() {
    Option o = new Option("verbose", "v",NONE,false,"a simple description of verbose");
    assertThat("--verbose".matches(o.getNameRegex())).isTrue();
  }

  @Test
  public void testOptionSingleParameter() {
    Option o = new Option("source", "src",STRING,false,"a simple description of source");
    assertThat("-src dir1/file1.txt -c"
        .replaceFirst(o.getAliasRegex(),"")
    ).isEqualTo(" -c");
  }

  @Test
  public void testOptionMultiParameter() {
    Option o = new Option("source", "src",STRING,true,"a simple description of source");
    assertThat("-src dir1/file1.txt \"dir2/file2.txt\" -c"
        .replaceFirst(o.getAliasRegex(),"")
    ).isEqualTo(" -c");
  }

  @Test
  public void testDefaultOptionRegex() {
    Option opt = new DefaultOption(STRING,"");
    SoftAssertions soft = new SoftAssertions();
    soft.assertThat(opt.getAliasRegex()).isEqualTo(opt.getNameRegex());
    soft.assertThat(opt.getAliasRegex()).isNotNull();
    soft.assertAll();
  }

  @Test
  public void testWrongDefaultOption() {
    assertThatThrownBy(() -> new DefaultOption(NONE,""))
        .isInstanceOf(IllegalArgumentException.class);
  }
}
