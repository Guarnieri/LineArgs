package lineargs;

import static lineargs.Program.concat;
import static lineargs.Types.dateOfString;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

public class TypesTest {

  @Test
  public void testWrongDateOfString() {
    assertThat(dateOfString("yyyy-MM-dd", "2018 6 15")).isNull();
  }

  @Test
  public void testLexerOptionConcat() {
    assertThat(concat(new String[]{})).isEqualTo("");
  }

}
