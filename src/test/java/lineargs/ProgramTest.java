package lineargs;

import static lineargs.Types.DOUBLE;
import static lineargs.Types.INTEGER;
import static lineargs.Types.NONE;
import static lineargs.Types.STRING;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import lineargs.printer.DefaultPrinter;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ProgramTest {
  Program program;

  @BeforeEach
  public void setProgram() {
    program = new Program("Name", "Description",
        new String[] {
            "--source",
            "dir1/file1.txt",
            "\"dir2/file2.txt\"",
            "-v", "-s", "1", "2", "3"
        }) {
      @Override
      public List<Option> getAllOptions() {
        ArrayList<Option> ops = new ArrayList<>();
        ops.add(new Option("source","src",STRING,true,""));
        ops.add(new Option("verbose","v",NONE,false,""));
        ops.add(new Option("sum","s",INTEGER,true,""));
        return ops;
      }
    };
  }

  @Test
  public void testProgram() {
    SoftAssertions soft = new SoftAssertions();
    soft.assertThat(program.isParameterSet("v")).isTrue();
    List<Object> l = program.getParameterValues("sum");
    soft.assertThat((int)l.get(0) + (int)l.get(1) + (int)l.get(2)).isEqualTo(6);
    soft.assertAll();
  }

  @Test
  public void testSetPrinter() {
    String help = program.getHelpMessage();
    program.setPrinter(
        new DefaultPrinter(program.name, program.description, program.getAllOptionWithHelp())
    );
    assertThat(program.getHelpMessage()).isEqualTo(help);
  }

  @Test
  public void testOrderedList() {
    List<Option> ops = program.getAllOptionWithHelp();
    SoftAssertions soft = new SoftAssertions();
    soft.assertThat(ops.get(0).getAlias()).isEqualTo("h");
    soft.assertThat(ops.get(1).getAlias()).isEqualTo("s");
    soft.assertThat(ops.get(2).getAlias()).isEqualTo("src");
    soft.assertThat(ops.get(3).getAlias()).isEqualTo("v");
    soft.assertAll();
  }

  @Test
  public void testGetHelpMessageNotNull() {
    assertThat(program.getHelpMessage()).isNotNull();
  }

  @Test
  public void testProgramWithDefaultParameter() {
    program = new Program("Name", "Description",
        new String[] {
            "hello world!",
            "3.14",
            "-s", "1", "2", "3",
            "Lorem Ipsum"
        }) {
      @Override
      public List<Option> getAllOptions() {
        ArrayList<Option> ops = new ArrayList<>();
        ops.add(new Option("sum","s",INTEGER,true,""));
        ops.add(new DefaultOption(DOUBLE,""));
        ops.add(new DefaultOption(STRING,""));
        return ops;
      }
    };
    SoftAssertions soft = new SoftAssertions();
    soft.assertThat(program.isParameterSet("sum")).isTrue();
    List<Object> nums = program.getParameterValues("sum");
    soft.assertThat((int)nums.get(0) + (int)nums.get(2)).isEqualTo(4);
    soft.assertThat(program.getDefaultValuesOfType(STRING)).hasSize(2);
    soft.assertThat(program.getDefaultValuesOfType(DOUBLE)).hasSize(1);
    soft.assertAll();
  }

  @Test
  public void testIsDefaultValueOfTypeSet() {
    program = new Program("Name", "Description",
        new String[] {
            "12",
            "4.6",
            "-s", "1", "2", "3"
        }) {
      @Override
      public List<Option> getAllOptions() {
        ArrayList<Option> ops = new ArrayList<>();
        ops.add(new Option("sum","s",INTEGER,true,""));
        ops.add(new DefaultOption(INTEGER,""));
        ops.add(new DefaultOption(DOUBLE,""));
        ops.add(new DefaultOption(STRING,""));
        return ops;
      }
    };
    SoftAssertions soft = new SoftAssertions();
    soft.assertThat(program.isDefaultValueOfTypeSet(INTEGER)).isTrue();
    soft.assertThat(program.isDefaultValueOfTypeSet(DOUBLE)).isTrue();
    soft.assertThat(program.isDefaultValueOfTypeSet(STRING)).isFalse();
    soft.assertAll();
  }
}
