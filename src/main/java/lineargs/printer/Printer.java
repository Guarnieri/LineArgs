package lineargs.printer;

/**
 * An object that formats and prints the help message associated to the executed program.
 */
public interface Printer {
  /**
   * Produces a formatted help message.
   * @return a formatted string that represent the help message of the executed program.
   */
  String prettyPrint();
}
